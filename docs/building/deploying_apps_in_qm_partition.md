# Deploying applications in the QM partition

The AutoSD architecture isolates quality managed (QM) applications from Automotive Safety Integrity Level (ASIL) applications. This isolation
mechanism is the QM software partition. In this architecture, the QM partition is an imageless container that uses a subsection of the host
filesystem as its root filesystem. This means that you must place your applications in one of two locations:

* Configure OSBuild to copy ASIL applications to `/etc/containers/systemd`
* Configure OSBuild to copy QM applications to `/etc/containers/systemd` inside the QM software partition, which is mounted at `/usr/share/qm/`.
  The resulting path for QM applications is `/usr/share/qm/etc/containers/systemd`.

To configure the placement of applications in the QM partition, create a new `qm_rootfs` pipeline in your manifest file, and configure the
QM-specific stages to perform operations in the QM partition. Specifically, configure the new `qm_rootfs` pipeline to call the preconfigured
`qm_rootfs_base` pipeline from the `qm.ipp.yml` file. This pipeline contains the instructions to build the root file system for the QM
partition. This file system is then mounted at `/usr/share/qm/` when OSBuild assembles the operating system (OS).

## Embedding RPM packages in the QM partition

Place all QM-level RPM packages in the QM partition.

**Prerequisites**

* An RPM package of your application, named `auto-apps`, in an RPM package repository located in the `/var/tmp/my_repo` directory and built
according to [Packaging applications with RPM](packaging_apps_with_rpm.md#packaging-applications-with-rpm)
* A custom manifest file, such as the file that you created in
[Embedding RPM packages in the AutoSD image](packaging_apps_with_rpm.md#embedding-rpm-packages-in-the-autosd-image)

**Procedure**

1. To configure your custom OSBuild manifest to install your `auto-apps` application into the QM partition of the OS image, set `use_qm: true` in
the `mpp-vars` section of your manifest file, and define your `auto-apps` RPM package in the `qm_extra_rpms` variable:

     ```console
     mpp-vars:
       name: <my-manifest>
       use_qm: true
       qm_extra_rpms:
         - auto-apps
     ```

       When you set `use_qm: true`, OSBuild invokes another manifest file which contains the `qm_rootfs_base` pipeline that configures the QM
       partition.

1. Create a new `qm_rootfs` pipeline in your manifest file and include the following snippet to reference the `qm_rootfs_base` pipeline:

     ```console
     - name: qm_rootfs
       build: name:build
       stages:
       - type: org.osbuild.copy
         inputs:
           tree:
             type: org.osbuild.tree
             origin: org.osbuild.pipeline
             references:
               - name:qm_rootfs_base
     ```

**Next steps**

Now that you have configured your custom OSBuild manifest to initialize the QM partition and install your RPM package, you can build your AutoSD
image. For more information, see [Building an AutoSD image](building_an_os_image.md#building-an-autosd-image).

**Additional resources**

* [Sample custom OSBuild manifest](ref_sample-yaml.md#sample-custom-osbuild-manifest)

## Embedding containerized applications in the QM partition

Place all QM-level containerized applications in the QM partition.

**Prerequisites**

* The ID of your `auto-apps` container image that you retrieved in
[Embedding containerized applications in the AutoSD image](containerizing_apps.md#embedding-containerized-applications-in-the-autosd-image)
* A custom manifest file, such as the file that you created in
[Embedding RPM packages in the AutoSD image](packaging_apps_with_rpm.md#embedding-rpm-packages-in-the-autosd-image)

**Procedure**

1. Create a new pipeline, named `qm_rootfs`, in your manifest file to reference the `qm_rootfs_base` pipeline, which configures the QM partition:

     ```console
     - name: qm_rootfs
       build: name:build
       stages:
       - type: org.osbuild.copy
         inputs:
           tree:
             type: org.osbuild.tree
             origin: org.osbuild.pipeline
             references:
               - name:qm_rootfs_base
        options:
          paths:
            - from: input://tree/
              to: tree:///
     ```

1. Configure the new `qm_rootfs` pipeline to extract the container image into the QM partition of your OS image at build time:
      1. Include the ID of the container image in a new `org.osbuild.skopeo` stage in the `qm_rootfs` pipeline of your manifest file:

        ```console
        - type: org.osbuild.skopeo
          inputs:
            images:
              type: org.osbuild.containers-storage
              origin: org.osbuild.source
              references:
                sha256<your-container-image-ID>:
                  name: localhost/auto-apps:latest
        ```

      2. Include the `containers-storage` option to place the container image in `/usr/share/containers/storage`, rather than the default,
      `/var/lib/containers/storage`:

        ```console
        - type: org.osbuild.skopeo
            inputs:
              images:
                type: org.osbuild.containers-storage
                origin: org.osbuild.source
                references:
                  sha256:&lt;your-container-image-ID>:
                    name: localhost/auto-apps:latest
            options:
              destination:
                type: containers-storage
                storage-path:
                  mpp-eval: containers_extra_store
        ```

1. To configure your custom OSBuild manifest to copy your container configuration files to `/usr/share/qm/etc/containers/systemd`, create a new
`org.osbuild.copy` stage inside the `qm_rootfs` pipeline of the manifest file:

     ```console
     - type: org.osbuild.copy
       inputs:
         inlinefile1:
           type: org.osbuild.files
           origin: org.osbuild.source
           mpp-embed:
             id: radio.container
             path: ../files/radio.container
         inlinefile2:
           type: org.osbuild.files
           origin: org.osbuild.source
           mpp-embed:
             id: engine.container
             path: ../files/engine.container
       options:
         paths:
     - from:
         mpp-format-string: input://inlinefile1/{embedded['radio.container']}
       to: tree:///usr/share/qm/etc/containers/systemd/radio.container
     - from:
         mpp-format-string: input://inlinefile2/{embedded['engine.container']}
       to: tree:///usr/share/qm/etc/containers/systemd/engine.container
     ```

    !!! note
        The `path:` option resolves a relative path. In this example, your
        container configuration files are in the `../files` directory.

**Next steps**

Now that you have included your containerized application in your OSBuild manifest, you can build your AutoSD image. For more information, see
[Building an AutoSD image](building_an_os_image.md#building-an-autosd-image).

**Additional resources**

* [Sample custom OSBuild manifest](ref_sample-yaml.md#sample-custom-osbuild-manifest)
