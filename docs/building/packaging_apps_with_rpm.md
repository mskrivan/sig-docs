# Packaging applications with RPM

To build and deploy applications for AutoSD, package the application with RPM Package Manager (RPM) and embed the packaged application in the
operating system (OS) image. The Automotive SIG provides sample OS image manifests that you can customize for specific use cases and sample
applications that you can package and deploy in your OS images.

There are two types of packages:

* Binary packages (RPM packages): A binary package contains compiled software.
* Source RPM packages (SRPM packages): A source package contains the source code of the application, the spec file that specifies how to compile the
source code, and eventual patches to produce the binary packages.

In this example workflow, you will learn how to prepare the sample application source code that you want to package with RPM, create a spec file,
build a binary RPM, and create a local RPM repository. Then configure the OS image build process to include your RPM at build time. To learn more
about RPM packages, see the
[RHEL RPM guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/packaging_and_distributing_software/packaging-software_packaging-and-distributing-software).

## Creating the RPM packaging workspace

To create an RPM package, you must first create a separate directory for your RPM packaging workspace. An RPM package consists of an archive of
source code and a spec file. The spec file contains project metadata and different sections that you use to define how to compile project source
code.

**Prerequisites**

* A host machine that runs on CentOS Stream, Fedora, or RHEL

**Procedure**

1. Install the tools required to build RPM packages, as well as dependencies required by the sample application:

      ```console
      sudo dnf install rpmdevtools rpm-build rpm-devel cmake make gcc-c++
      boost-devel vsomeip3-devel
      ```

2. From your `home` directory, run the `rpmdev-setuptree` utility to create a file structure for the RPM packaging workspace:

      ```console
      cd && rpmdev-setuptree
      ```

1. Verify that `rpmdev-setuptree` created the required directories:

      ```console
      $ tree ~/rpmbuild/
      /home/<user>/rpmbuild/
      |-- BUILD
      |-- RPMS
      |-- SOURCES
      |-- SPECS
      |-- SRPMS

      5 directories, 0 files
      ```

The required directories each serve a unique purpose:

* `BUILD`: The location of various `%buildroot` directories. These are useful for investigating a failed build if the log output is inconclusive.
* `RPMS`: The location of your binary RPM packages. This directory contains subdirectories for different architectures.
* `SOURCES`: The location of your source code. Package your source code in a `tar` archive and copy it to the `SOURCES` directory. At build time, the
`rpmbuild` build tool extracts your software from this directory.
* `SPECS`: The location of your spec files.
* `SRPMS`: The location of your SRPM packages.

## Packaging sample application source code with RPM

Use this workflow to package a sample application that is available [upstream](https://gitlab.com/CentOS/automotive/src/sample-apps). The sample
application has several services:

* `engine-service`: A service with a single event that signals when the car is in reverse.
* `radio-service`: A service that emulates a radio, regularly publishing information about the current song, radio station, and volume. It accepts
requests to turn the radio on and off, change the channel, and adjust the volume. If the engine service is available, the `radio-service` listens
for events and temporarily lowers the radio volume while the car is in reverse.
* `radio-client`: A command line program that displays the current state of the radio service and enables you to control it. The keyboard controls
are displayed on the screen.

**Procedure**

1. Clone the sample application repository:

      ```console
      git clone git@gitlab.com:CentOS/automotive/src/sample-apps.git
      cd sample-apps
      ```

2. Create a `tar` archive using upstream samples:

      ```console
      git archive HEAD . --prefix=auto-apps-0.1/ --output=auto-apps-0.1.tar.gz
      ```

3. Move the `.tar` archive of your software to the `rpmbuild/SOURCES` directory:

      ```console
      mv sample-images/sample-apps/auto-apps-0.1.tar.gz rpmbuild/SOURCES/
      ```

4. From the `~/rpmbuild/SPECS/` directory, create a `spec` file for a new RPM package called `auto-apps`:

      ```console
      rpmdev-newspec auto-apps
      ```

      This command creates a spec file named `auto-apps.spec`.

1. Modify the `~/rpmbuild/SPECS/auto-apps.spec` file with a text editor:

      ```console
      Name:           auto-apps
      Version:        0.1
      Release:        1%{?dist}
      Summary:        A test RPM of auto-apps

      License:        MIT
      Source0:        auto-apps-%{version}.tar.gz

      BuildRequires:  cmake make gcc-c++ boost-devel vsomeip3-devel

      %description
      Sample auto applications

      %prep
      %autosetup

      %build
      %cmake
      %cmake_build

      %install
      %cmake_install

      %files
      %{_bindir}/engine-service
      %{_bindir}/radio-client
      %{_bindir}/radio-service

      %changelog
      * Tue Feb 20 2024 your_name <your_email>
      - Initial packaging
      ```

      Be sure to update the `%build`, `%install`, and `%files` sections of the spec file. This spec file is a minimal working spec file for the
      sample `auto-apps` applications. You can further customize the spec file to control your RPM build process. For more information about spec
      files and how to customize them, see the [RPM packaging Guide](https://rpm-packaging-guide.github.io/#what-is-an-rpm) on GitHub.

2. Build the binary RPM:

      ```console
      rpmbuild -ba ~/rpmbuild/SPECS/auto-apps.spec
      ```

3. Create a directory for your RPM package repository, move your `.rpm` file to this directory, and initialize the directory as an RPM package
repository:

      ```console
      mkdir /var/tmp/my_repo
      cp -rp ~/rpmbuild/RPMS/* /var/tmp/my_repo/
      createrepo /var/tmp/my_repo
      ```

Your RPM package repository is now ready, and it contains your software packaged as a `.rpm` file. When you build the AutoSD image, include the RPM
package repository in the build manifest to enable the OS image to incorporate your software.

## Packaging your application source code with RPM

Use this workflow to package your own application source code into an RPM package. After you package your software, add it to a local RPM repository.
RPM Package Manager queries the RPM packages in this repository, resolves their dependencies, and installs them in your operating system (OS) image.

1. Create a `tar` archive of your application source code:

      ```console
      tar -cvf my-app.tar.gz my-app
      ```

      In this example, your application source code is in a directory named `my-app`.

1. Move the `.tar` archive of your software to the `rpmbuild/SOURCES` directory:

      ```console
      mv my-app.tar.gz rpmbuild/SOURCES/
      ```

1. Create a `spec` file for a new RPM package called `my-app`:

      ```console
      rpmdev-newspec my-app
      ```

      This command creates a spec file for your `my-app` application in `~/rpmbuild/SPECS/my-app.spec`.

1. Modify the `~/rpmbuild/SPECS/my-app.spec` file with a text editor. For more information about spec files and how to customize them, see the
[RPM packaging Guide](https://rpm-packaging-guide.github.io/#what-is-an-rpm) on GitHub.

1. Build the binary RPM package:

      ```console
      rpmbuild -ba ~/rpmbuild/SPECS/my-app.spec
      ```

1. Create a directory for your RPM package repository, move your `.rpm` file to this directory, and initialize the directory as an RPM package
repository:

      ```console
      mkdir /var/tmp/my_repo
      cp -rp ~/rpmbuild/RPMS/* /var/tmp/my_repo/
      createrepo /var/tmp/my_repo
      ```

Your RPM package repository is now ready, and it contains your software packaged as a `.rpm` file. When you build the AutoSD image, include the RPM
package repository in the build manifest to enable the OS image to incorporate your software. For more information about building the AutoSD image,
see [Building an AutoSD image](building_an_os_image.md#building-an-autosd-image).

* [Packaging_Tutorial_GNU_Hello/](https://docs.fedoraproject.org/en-US/package-maintainers/Packaging_Tutorial_GNU_Hello/)
* [CentOS/automotive/src/sample-apps](https://gitlab.com/CentOS/automotive/src/sample-apps)
* [RPM Packaging Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/packaging_and_distributing_software/packaging-software_packaging-and-distributing-software#building-rpms_packaging-software)

## Embedding RPM packages in the AutoSD image

Embed your RPM-packaged application in the AutoSD image with the `automotive-image-builder` tool. This tool leverages OSBuild to pull your
application from your RPM package repository at build time, automatically resolving and installing the dependencies for your RPM package.

**Prerequisites**

* An RPM package `auto-apps` in an RPM repository `/var/tmp/my_repo`

**Procedure**

1. Create a new `.mpp.yml` OSBuild manifest file. This file will contain your custom configuration for your AutoSD image:

      ```console
      touch <my-manifest>.mpp.yml
      ```

1. In `<my-manifest>.mpp.yml`, create a `rootfs` pipeline with an `org.osbuild.rpm` stage that identifies `auto-apps` and the path to the RPM package
repository:

      ```console
      version: '2'
      mpp-vars:
      name: <my-manifest>

      pipelines:
      - name: rootfs
      build: name:build
      stages:
      - type: org.osbuild.rpm
         options:
            gpgkeys:
            - mpp-eval: distro_gpg_keys
            disable_dracut: true
            exclude:
            docs: true
         inputs:
            packages:
            type: org.osbuild.files
            origin: org.osbuild.source
            mpp-depsolve:
               architecture: $arch
               ignore-weak-deps: true
               module-platform-id: $distro_module_id
               baseurl: $distro_baseurl_repo
               repos:
                  mpp-join:
                  - mpp-eval: image_repos
                  - mpp-eval: extra_repos
                  - - id: auto-apps
                     baseurl: file:///var/tmp/my_repo
               packages:
                  mpp-join:
                  - mpp-eval: image_rpms
                  - mpp-eval: extra_rpms
                  - - auto-apps
               excludes:
               - dracut-config-rescue
      ```

When you build your operating system (OS) image, OSBuild installs the `auto-apps` RPM in the default location in your OS image. This default location
is for ASIL applications. For more information about installing RPM packages in the QM partition of your OS image, see
[Embedding RPM packages in the QM partition](deploying_apps_in_qm_partition.md#embedding-rpm-packages-in-the-qm-partition).

**Next steps**

* Now that you included your RPM package repository and RPM package application in a new custom OSBuild manifest, you can build your AutoSD image.
For more information, see [Building an AutoSD image](building_an_os_image.md#building-an-autosd-image).
* Alternatively, you can continue customizing your image. For more information, see
[Containerizing applications](containerizing_apps.md#containerizing-applications).

**Additional resources**

* [Sample custom OSBuild manifest](ref_sample-yaml.md#sample-custom-osbuild-manifest)

## Embedding RPM packages from remote repositories into the AutoSD image

Configure your manifest file to embed RPM packages from the remote repository of a Linux distribution. The `automotive-image-builder` tool uses this
manifest to automatically resolve and embed your remote applications and their dependencies into your AutoSD image at build time.

**Prerequisites**

* One or more base URLs for any content repository that has application RPM packages

**Procedure**

1. Open the manifest file you created in
[Embedding RPM packages in the AutoSD image](packaging_apps_with_rpm.md#embedding-rpm-packages-in-the-autosd-image), or create a new OSBuild
manifest file to contain your custom configuration for your AutoSD image:

      ```console
      touch <my-manifest>.mpp.yml
      ```

1. Create a `rootfs` pipeline with an `org.osbuild.rpm` stage:

      ```console
      version: '2'
      mpp-vars:
        name: <my-manifest>

      pipelines:
      - name: rootfs
        build: name:build
        stages:
        - type: org.osbuild.rpm
          options:
            gpgkeys:
            - mpp-eval: distro_gpg_keys
            disable_dracut: true
            exclude:
              docs: true
      ```

2. Define one or more remote repositories and one or more application RPM packages:

      ```console
         inputs:
           packages:
             type: org.osbuild.files
             origin: org.osbuild.source
             mpp-depsolve:
               architecture: $arch
               ignore-weak-deps: true
               module-platform-id: $distro_module_id
               baseurl: $distro_baseurl_repo
               repos:
                 mpp-join:
                 - mpp-eval: image_repos
                 - mpp-eval: extra_repos
                 - - id: epel
                   - name: Extra Packages for Enterprise Linux
                   - baseurl: https://dl.fedoraproject.org/pub/epel/$version/Everything/$arch/
                   - gpgkey=distro_gpg_keys
               packages:
                 mpp-join:
                 - mpp-eval: image_rpms
                 - mpp-eval: extra_rpms
                 - - _<package-name>_
               excludes:
               - dracut-config-rescue
      ```

**Next steps**

* Add locally stored ASIL automotive applications to your custom manifest. For more information, see [Embedding RPM packages in the AutoSD image](packaging_apps_with_rpm.md#embedding-rpm-packages-in-the-autosd-image).
* Add locally stored QM automotive applications to your custom manifest. For more information, see
[Deploying applications in the QM partition](deploying_apps_in_qm_partition.md#deploying_apps_in_qm_partition).
* Add containerized applications to your custom manifest. For more information, see
[Containerizing applications](containerizing_apps.md#containerizing_apps).
* When you finish configuring your OSBuild manifest, you can build your {ProductShortName} image. For more information, see
[Building an AutoSD image](building_an_os_image.md#building_an_os_image).

**Additional resources**

* [Sample custom OSBuild manifest](ref_sample-yaml.md#sample-custom-osbuild-manifest)
