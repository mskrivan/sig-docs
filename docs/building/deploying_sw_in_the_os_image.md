# Deploying software on AutoSD

Deploying third-party software on an automotive hardware platform running AutoSD is an incremental process. During the process, you use tools to
package pplication source code and spec files. Then you embed the resulting packages in the operating system (OS) image by using a build manifest
file. You can embed applications into AutoSD by using RPM Package Manager (RPM) packages or containers. The process is similar for both Automotive
Safety Integrity Level (ASIL) and quality managed (QM) applications.

OSBuild installs prebuilt packages, in the form of RPMs, into the OS image. You specify the content of the AutoSD image in a target-specific input
file by selecting the set of packages to install, as well as the configuration settings.

An automotive build manifest is a `YAML` file that contains a generic definition of an image. This file is given as input to the
`automotive-image-builder` tool. The `automotive-image-builder` tool combines the `YAML` file, a set of internal configuration files, and the
current state of any package repositories or container registries used into a fully resolved OSBuild input file. When you run the
`automotive-image-builder` tool, you can also specify options that affect how the image is built. These options range from global settings, such as
the architecture and which board to build for, to details such as which extra packages to include.

Example manifests for {ProductName} exist in the project hosted in the Automotive SIG
[sample-images repository](https://gitlab.com/CentOS/automotive/sample-images).

The `osbuild-manifests` directory contains various manifest files with the extensions `mpp.yml`. The `osbuild-manifests` directory also contains a
`Makefile`, which is a script that you can use to streamline the process of building images.

The image build process pulls these packages from local or remote package repositories.

AutoSD packages are available in standard package repositories that Red Hat maintains, and you can pull additional software from other public or
private package repositories.

In the software compilation process, there is a distinction between the host and target environments:

* Host environment: where you compile the software
* Target environment: where the software runs

In a native, self-hosted software development environment, the host and target environments are the same. In a self-hosted environment, the host
system’s native compiler compiles the source code. The resulting binaries are linked against the native shared libraries of the system. The
resulting artifacts can run on the originating host system and on any compatible target systems, which are those with the same hardware architecture,
user space, kernel ABIs, and set of shared libraries.

Applications that you plan to integrate into an AutoSD image must be:

* Built in a way that is compatible with AutoSD
* Packaged in a way that is compatible with OSBuild
* Stored in a package repository that OSBuild can access

**Application build workflow**

![Application build workflow depicts the application build environment](../img/app-build-workflow.jpg)
