# Configuring communication between QM and ASIL containers

Cross-environment communication works similar to communication within the QM partition. In addition to mounting a volume, or storage area, that
contains a UNIX domain socket inside the quality-managed (QM) container, you must create another UNIX domain socket in the Automotive Safety
Integrity Level (ASIL) partition to enable inter-process communication (IPC) between the QM and ASIL partitions.

**Prerequisites**

* A custom manifest file, such as the manifest file that you created in
[Configuring communication between QM containers](configuring_communication_qm_containers.md#configuring-communication-between-qm-containers)
* An ASIL container and a QM container that you want to communicate with each other

**Procedure**

1. To configure IPC between the two containers, update the Quadlet configuration to mount the volumes that contain the UNIX domain sockets in
`/var/run/<another-dir>`. In the example code, `<your-dir>` is `asil-ipc-demo`, but you can name the directory according to your own conventions.

    1. To connect the `engine` application in the ASIL layer with `my-client-app` in the QM layer, add this line to the `engine.container` file to
    mount the volume where the ASIL socket resides in the container, and set the correct SELinux label:

        ```console
        [Container]
        Volume=/run/ipc/asil:/run/ipc/asil
        SecurityLabelType=ipc_t
        ```

    2. Add this line to the `my-client-app.container` file to mount the volume where the ASIL socket resides in the container:

        ```console
        [Container]
        Volume=/run/ipc/asil:/run/ipc/asil
        SecurityLabelType=qm_container_ipc_t
        ```

1. Create a Quadlet drop-in configuration for the QM container in the `files/root_fs/qm.container.d/10-extra-volume.conf` file:

    ```console
    [Unit]
    Requires=ipc.socket

    [Container]
    Volume=/run/ipc/asil:/run/ipc/asil
    ```

    !!! note
        QM configuration is built in to the partition and should not be changed. Using drop-in files allows you to make additional modifications to
        the QM partition without overwriting the inherent configuration settings.

1. Copy the drop-in Quadlet QM configuration file to the image:

    1. Add a new `org.osbuild.mkdir` stage in the `rootfs` pipeline of your manifest file:

        ```console
        - type: org.osbuild.mkdir
          options:
            paths:
              - path: /etc/containers/systemd/qm.container.d
                exist_ok: true
                parents: true
        ```

    2. Add a new input item to the `org.osbuild.copy` stage in the `rootfs` pipeline of your manifest file:

        ```console
        - type: org.osbuild.copy
          inputs:
            [...]
            inlinefile4:
              type: org.osbuild.files
              origin: org.osbuild.source
              mpp-embed:
                id: qm_extra_volume
                path: ../files/root_fs/qm.container.d/10-extra-volume.conf

          options:
            paths:
            [...]
            - from:
                mpp-format-string: input://inlinefile4/{embedded['qm_extra_volume']}
              to: tree:///etc/containers/systemd/qm.container.d/10-extra-volume.conf
        ```

2. Optional: To use `systemd` to create a UNIX socket file, create a `*.socket` file with the same name as the service with which the socket is
associated, such as `engine.socket` for the engine service.

    1. Create the `systemd` socket file in `files/root_fs/engine.socket`:

        ```console
        [Unit]
        Description=An example systemd unix socket

        [Socket]
        ListenStream=%t/ipc/asil/ipc.socket
        RuntimeDirectory=ipc/asil

        [Install]
        WantedBy=sockets.target
        ```

        !!! note
            This step is optional because server application that binds and activates the UNIX socket can manage socket creation. However, `systemd`
            manages socket creation natively, which ultimately helps service synchronization.

    2. To enable the application to start after you create the socket, add these lines to the `engine.container` file to create a dependency with the
    `systemd` socket service:

        ```console
        [Unit]
        Requires=engine.socket
        After=engine.socket
        ```

    3. To copy the `systemd` socket file for the QM container into the image, add a new input item to the `org.osbuild.copy` stage in the `rootfs`
    pipeline of your manifest file:

        ```console
        - type: org.osbuild.copy
          inputs:
          [...]
            inlinefile5:
              type: org.osbuild.files
              origin: org.osbuild.source
              mpp-embed:
                id: engine_socket
                path: ../files/root_fs/engine.socket

          options:
            paths:
            [...]
            - from:
                mpp-format-string: input://inlinefile5/{embedded['engine_socket']}
              to: tree:///etc/systemd/system/engine.socket
        ```

1. Optional: Set the `SELinux` configuration to `permissive` in the OSBuild manifest:

    ```console
    - type: org.osbuild.selinux.config
      options:
        state: permissive
    ```

**Additional resources**

* [Sample custom OSBuild manifest](ref_sample-yaml.md#sample-custom-osbuild-manifest)
