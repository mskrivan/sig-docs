# Configuring communication between ASIL containers

Inter-process communication (IPC) allows processes, such as applications and services, to share data and synchronize resources through a common channel.
Applications that communicate using IPC are either servers or clients. Servers actively listen to requests in the channel and respond to them.
Clients connect to the channel to request data. Configuration stages related to ASIL applications belong in the `rootfs` pipeline of the manifest
file, whereas configuration stages related to QM applications belong in the `qm_rootfs` pipeline.

Signals, shared files, network sockets, and D-Bus subsystems are mechanisms in the kernel that enable IPC. In AutoSD, IPC occurs by using UNIX
domain sockets. UNIX domain sockets are virtual sockets similar to network sockets, but UNIX domain sockets are constrained to the same host
operating system.

UNIX domain sockets use the file system as their address name space. The sockets are seen as files for other processes and can open the same socket
to communicate. UNIX sockets do not use the file system to exchange data. They use the kernel’s memory buffers instead.

In this context, to enable IPC between ASIL applications, the applications' containers require access to the same UNIX domain socket in the file
system. The volume, or storage area, that contains the UNIX domain socket must be mounted inside the containers. For more information about socket
creation, see [Systemd sockets](https://www.freedesktop.org/software/systemd/man/latest/systemd.socket.html).

**Prerequisites**

* A custom manifest file, such as the manifest file that you created in
[Embedding RPM packages in the AutoSD image](packaging_apps_with_rpm.md#embedding-rpm-packages-in-the-autosd-image)
* Two or more containers embedded in your image that you want to configure to communicate. In this example, these containers are the `radio-service`
and `engine-service` containers from the [sample apps](https://gitlab.com/CentOS/automotive/src/sample-apps) repository.

**Procedure**

1. To configure communication between the `radio-service` and `engine-service` containers, mount the volumes that contain the UNIX domain sockets in
`/var/run/<your-dir>`. In the example code, `<your-dir>` is `asil-ipc-demo`, but you can name the directory according to your own conventions.

      1. Add this line to the `engine.container` file to mount the volume where the ASIL socket resides:

        ```console
        [Container]
        Volume=/run/asil-ipc-demo:/run/asil-ipc-demo
        ```

      2. Add this line to the `radio.container` file to mount the volume where the ASIL socket resides:

        ```console
        [Container]
        Volume=/run/asil-ipc-demo:/run/asil-ipc-demo
        ```

1. Optional: Use `systemd` to create a UNIX socket file with the same name as the service with which the socket is associated, such as the `radio`
service.

      1. Create the `systemd.socket` file in `files/root_fs/radio.socket`:

        ```console
        [Unit]
        Description=An example systemd unix socket

        [Socket]
        ListenStream=%t/asil-ipc-demo/asil_ipc.socket
        RuntimeDirectory=asil-ipc-demo

        [Install]
        WantedBy=sockets.target
        ```

      2. To enable the application to start after you create the socket, add these lines to the `radio.container` file to create a dependency with
      the `systemd` socket service:

         ```console
         [Unit]
         After=radio.socket
         Requires=radio.socket
         ```

      3. To copy the `radio.socket` file to the `/etc/systemd/system/` directory, create a new `org.osbuild.copy` stage in the `rootfs` pipeline of
      your manifest file:

         ```console
         - type: org.osbuild.copy
             inputs:
               inlinefile3:
                 type: org.osbuild.files
                 origin: org.osbuild.source
                 mpp-embed:
                   id: radio_socket
                   path: ../files/root_fs/radio.socket

             options:
                paths:
                - from:
                    mpp-format-string:
                    input://inlinefile3/{embedded['radio_socket']}
                  to: tree:///etc/systemd/system/radio.socket
         ```

      1. Enable the service to ensure that the socket is created at boot. Create a new `org.osbuild.systemd` stage in the `rootfs` pipeline of your
      manifest file:

         ```console
         - type: org.osbuild.systemd
           options:
             enabled_services:
             - radio.socket
         ```

**Next steps**

* Now that you configured communication between ASIL containers, you can build your AutoSD image. For more information, see
[Building an AutoSD image](building_an_os_image.md#building-an-autosd-image).
* Alternatively, you can continue customizing your image. For more information, see
 [Containerizing applications](containerizing_apps.md#containerizing-applications).

**Additional resources**

* [Sample custom OSBuild manifest](ref_sample-yaml.md#sample-custom-osbuild-manifest)
