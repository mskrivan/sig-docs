# Running AutoSD on qemu

The Automotive SIG publishes images built via [OSBuild](https://www.osbuild.org/)
based on the OSBuild manifest present in the [sample-images](https://gitlab.com/CentOS/automotive/sample-images/)
repository.

Here is a quick guide on how to get you started with them. These steps should work on either Linux or macOS on the x86_64 or AArch64 CPU architectures.

1. Download your QEMU image from [https://autosd.sig.centos.org/AutoSD-9/nightly/](https://autosd.sig.centos.org/AutoSD-9/nightly/).
2. Unpack it.

      ```console
      unxz auto-osbuild-qemu-cs9-ps-regular-aarch64-602301589.3e284fb7.qcow2.xz
      ```

3. Boot the image by using the `runvm` script from the [sample-images](https://gitlab.com/CentOS/automotive/sample-images/) repo:

      ```console
      ./runvm auto-osbuild-qemu-cs9-ps-regular-aarch64-602301589.3e284fb7.qcow2
      ```

      To get a login prompt in the same shell, add the `--nographics` argument:

      ```console
      ./runvm --nographics auto-osbuild-qemu-cs9-ps-regular-aarch64-602301589.3e284fb7.qcow2
      ```

      If you run it on a different architecture specify the guest architecture with: `--arch aarch64`:

      ```console
      ./runvm --arch aarch64 auto-osbuild-qemu-cs9-ps-regular-aarch64-602301589.3e284fb7.qcow2
      ```

4. Log in as `root` or `guest` using the password: `password`.
