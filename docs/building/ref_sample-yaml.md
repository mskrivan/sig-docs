# Sample custom OSBuild manifest

Refer to this sample OSBuild manifest as a complete example of the configuration that you perform as part of the _Building applications for AutoSD_ workflow:

* One RPM installed in the default ASIL location.
* One RPM installed in the QM partition.
* One ASIL container.
* One QM container.

```console
version: '2'
mpp-vars:
  name: <my-manifest>
  use_containers_extra_store: true
  use_qm_containers_extra_store: true
  use_qm: true
  extra_rpms:
    - podman
    - podman-py
  qm_extra_rpms:
    - auto-apps

    extra_repos:
     - baseurl: https://download.copr.fedorainfracloud.org/results/rhcontainerbot/podman-next/epel-9-$arch/
       id: podman_next_copr

sources:
 org.osbuild.containers-storage:
   items:
     ## Run `podman inspect $image_name` and paste the id of the container image in this section (for each image)
     sha256:<your-container-image-ID>: {}
pipelines:
- mpp-import-pipelines:
    path: include/build.ipp.yml

- name: qm_rootfs
  build: name:build
  stages:
- type: org.osbuild.copy
    inputs:
      tree:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
- name:qm_rootfs_base
    options:
      paths:
- from: input://tree/
          to: tree:///
- type: org.osbuild.skopeo
    inputs:
      images:
        type: org.osbuild.containers-storage
        origin: org.osbuild.source
        references:
          # container id retrieved with `podman info $image_name`
          sha256:<your-container-image-ID>:
            # container name and tag to be used within the autosd image
            name: localhost/auto-apps:latest
    options:
      destination:
        type: containers-storage
        storage-path:
          mpp-eval: containers_extra_store
- type: org.osbuild.skopeo
    inputs:
      images:
        type: org.osbuild.containers
        origin: org.osbuild.source
        mpp-resolve-images:
          images:
- name: localhost/my-client-app:latest
            source: <your-container-registry>/my-client-app
            tag: latest
- type: org.osbuild.mkdir
    options:
      paths:
- path: /etc/containers/systemd
          exist_ok: true
- type: org.osbuild.copy
    inputs:
      qm_extra_files_0:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: qm_ipc_socket
          path: ../files/qm_fs/auto-apps.socket
      qm_extra_content_1:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: qm_auto_apps_container
          path: ../files/qm_fs/auto-apps.container
      qm_extra_content_2:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: my_client_app_container
          path: ../files/qm_fs/my-client-app.container

      options:
        paths:
- from:
          mpp-format-string: input://qm_extra_files_0/{embedded['qm_ipc_socket']}
        to: tree:///etc/systemd/system/auto-apps.socket
- from:
          mpp-format-string: input://qm_extra_content_1/{embedded['qm_auto_apps_container']}
        to: tree:///etc/containers/systemd/auto-apps.container
- from:
          mpp-format-string: input://qm_extra_content_2/{embedded['my_client_app_container']}
        to: tree:///etc/containers/systemd/my-client-app.container
- type: org.osbuild.systemd
    options:
      enabled_services:
- auto-apps.socket
- name: rootfs
  build: name:build
  stages:
- type: org.osbuild.rpm
    options:
      gpgkeys:
- mpp-eval: distro_gpg_keys
      disable_dracut: true
      exclude:
        docs: true
    inputs:
      packages:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-depsolve:
          architecture: $arch
          ignore-weak-deps: true
          module-platform-id: $distro_module_id
          baseurl: $distro_baseurl_repo
          repos:
            mpp-join:
- mpp-eval: image_repos
- mpp-eval: extra_repos
- - id: auto-apps
                baseurl: file:///var/tmp/my_repo
          packages:
            mpp-join:
- mpp-eval: image_rpms
- mpp-eval: extra_rpms
- - auto-apps
          excludes:
- dracut-config-rescue
- type: org.osbuild.skopeo
    inputs:
      images:
        type: org.osbuild.containers-storage
        origin: org.osbuild.source
        references:
          # container id retrieved with `podman info $image_name`
          sha256:<your-container-image-ID>:
            # container name and tag to be used within the autosd image
            name: localhost/auto-apps:latest
    options:
      destination:
        type: containers-storage
        storage-path:
          mpp-eval: containers_extra_store
- type: org.osbuild.mkdir
    options:
      paths:
- path: /etc/containers/systemd/qm.container.d
          exist_ok: true
          parents: true
- type: org.osbuild.copy
    inputs:
      inlinefile1:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: radio.container
          path: ../files/radio.container
      inlinefile2:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: engine.container
          path: ../files/engine.container
      inlinefile3:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: radio_socket
          path: ../files/root_fs/radio.socket
      inlinefile4:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: qm_extra_volume
          path: ../files/root_fs/qm.container.d/10-extra-volume.conf
      inlinefile5:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: engine_socket
          path: ../files/root_fs/engine.socket
    options:
      paths:
- from:
          mpp-format-string: input://inlinefile1/{embedded['radio.container']}
        to: tree:///etc/containers/systemd/radio.container
- from:
          mpp-format-string: input://inlinefile2/{embedded['engine.container']}
        to: tree:///etc/containers/systemd/engine.container
- from:
          mpp-format-string: input://inlinefile3/{embedded['radio_socket']}
        to: tree:///etc/systemd/system/radio.socket
- from:
          mpp-format-string: input://inlinefile4/{embedded['qm_extra_volume']}
        to: tree:///etc/containers/systemd/qm.container.d/10-extra-volume.conf
- from:
          mpp-format-string: input://inlinefile5/{embedded['engine_socket']}
        to: tree:///etc/systemd/system/engine.socket
- type: org.osbuild.systemd
    options:
      enabled_services:
- radio.socket
- type: org.osbuild.users
    options:
      users:
        guest:
          password:
            mpp-eval: guest_password
          gid:
            mpp-eval: guest_gid
          uid:
            mpp-eval: guest_uid
- mpp-import-pipelines:
    path: include/image.ipp.yml
```
