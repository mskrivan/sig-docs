# Building an AutoSD image

The `automotive-image-builder` tool is a Python script that extends the functionality of the OSBuild tool to simplify how to create and customize
operating system (OS) images.

When you run the `automotive-image-builder` tool, it leverages OSBuild to build images based on `YAML` files called manifests. Manifests contain
specifications for your OS image. Manifests specify build details, such as which repositories and RPM packages to include, which services to enable,
and hostname and time zone configurations. Manifest files also describe stages that modify the file system of the image.

In this workflow, you have configured your own custom manifest file that you will use to build your own custom AutoSD image. For further reference,
you can also review the sample manifests that you can use to build images in the
[Automotive SIG repository](https://gitlab.com/CentOS/automotive/sample-images).

## Building and running your customized OS image

The `automotive-image-builder` repository includes a script that simplifies the build process. The `automotive-image-builder` script invokes OSBuild
to build the images by using manifest files that you specify.

**Prerequisites**

* A clone of the [automotive-image-builder](https://gitlab.com/CentOS/automotive/src/automotive-image-builder) repository
* Any customizations that you have made to the sample OSBuild manifest files, such as the manifest file that you created in
[Embedding RPM packages in the AutoSD image](packaging_apps_with_rpm.md#embedding-rpm-packages-in-the-autosd-image)
and extended in the other sections in this guide
* OSBuild

**Procedure**

1. Build the AutoSD image. In this example, specify your manifest file and the `qcow2` image format in the `automotive-image-builder build` command:

     ```console
     ./automotive-image-builder build --distro cs9 --mode package --target qemu \
     --export qcow2 <my-manifest>.mpp.yml <my-image>.qcow2
     ```

    !!! note
        If you build multiple images, run `dnf clean all` between builds to clear the build artifacts and reset the DNF cache.

1. Run the image:

     ```console
     ./automotive-image-runner <my-image>.qcow2
     ```

1. After the image boots, log in as `root` using the password `password`.
1. Verify that your packaged software is present in your image:

     ```console
     rpm -q auto-apps dlt-daemon vsomeip3-routingmanager
     ```

Your image is now ready.
